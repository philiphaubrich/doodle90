/* Philip Haubrich 
  Simple program mucking about with bezier curves
*/

//The headers
#include <time.h>
#include <unistd.h>
#include <math.h>
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"

#define false 0
#define true 1

#define STR 0 
#define MID 1
#define END 2

#define X 0
#define Y 1

#define TARGET_LENGTH 200

//The event structure
SDL_Event g_event;

//Screen attributes
const int SCREEN_WIDTH = 512;
const int SCREEN_HEIGHT = 512;
const int SCREEN_BPP = 32;
const int FRAMES_PER_SECOND = 20;
SDL_Surface *g_screen = NULL;     //Main surface displayed
SDL_Surface *g_message = NULL;    //Used for text

char g_buff[100];

//Font
TTF_Font *g_font;
SDL_Color textColor = { 254, 255, 255 }; // R, G, B

int g_seed;

int g_startTime;

const clock_t QUARTERSEC = CLOCKS_PER_SEC/4;
const clock_t MILLISEC = CLOCKS_PER_SEC/1000;
const clock_t FASTEST = 0;
clock_t g_speed = CLOCKS_PER_SEC/1000; // -d noDisplay  -f fastest (no delay) -s


//Big-ass datastructure
int g_lines[TARGET_LENGTH][3][2];


//Ugh, moving shit about so our control isn't in the animation loop. So these here are globals now
int workingPoint=1;
int infinateLoop = 0;

const float INCREMENT = 0.2;

int g_wheel = 10;

void pointOnLinef(float *targetX, float *targetY, float x1, float y1, float x2, float y2, float percent);

void pointOnLine(int *targetX, int *targetY, int x1, int y1, int x2, int y2, float percent);
int pointInLine(int *targetX, int *targetY, int x1, int y1, int x2, int y2, float percent);
void drawBox(SDL_Surface* surface, int x1,int y1,int x2,int y2, Uint32 color);
void drawText(int x, int y, SDL_Color color, TTF_Font* font, const char* text, ...);



int lineIntersect(float x1,float y1,float x2,float y2, float x3,float y3,float x4,float y4) 
{
  if(((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)) == 0 || ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)) == 0)
    return false;

  float x=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
  float y=((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
  if (x1>=x2) 
  {
    if (!(x2<=x&&x<=x1)) {return false;}
  } else {
    if (!(x1<=x&&x<=x2)) {return false;}
  }
  if (y1>=y2) {
    if (!(y2<=y&&y<=y1)) {return false;}
  } else {
    if (!(y1<=y&&y<=y2)) {return false;}
  }
  if (x3>=x4) {
    if (!(x4<=x&&x<=x3)) {return false;}
  } else {
    if (!(x3<=x&&x<=x4)) {return false;}
  }
  if (y3>=y4) {
    if (!(y4<=y&&y<=y3)) {return false;}
  } else {
    if (!(y3<=y&&y<=y4)) {return false;}
  }

  return true;
}


void testBezierQuad(float* fromX, float* fromY, float* toX, float* toY, int startX,int startY,int midX,int midY, int endX, int endY, float section)
{
  float line1x, line1y, line2x, line2y,  line3x, line3y;
  
  pointOnLinef(&line1x, &line1y, (float)startX, (float)startY, (float)midX, (float)midY, section);
  pointOnLinef(&line2x, &line2y, (float)midX, (float)midY, (float)endX, (float)endY, section);
  pointOnLinef(&line3x, &line3y, line1x, line1y, line2x, line2y, section);
  
  *fromX = line3x;
  *fromY = line3y;
  
  pointOnLinef(&line1x, &line1y, (float)startX, (float)startY, (float)midX, (float)midY, section + INCREMENT);
  pointOnLinef(&line2x, &line2y, (float)midX, (float)midY, (float)endX, (float)endY, section + INCREMENT);
  pointOnLinef(&line3x, &line3y, line1x, line1y, line2x, line2y, section + INCREMENT);
  
  *toX = line3x;
  *toY = line3y;


}


//Analyses selection to find how good of a choice it is.
//Ideally all lines only ever intersect at 90 degrees, and never get too close otherwise
int validateCurve(int testCurve)
{
  int startPoint = 0;
  float testSeg, targetSeg;
  int i;
  if(infinateLoop)
  {
    startPoint = (workingPoint == 99)? 0 : workingPoint+1;
  }
  else
  {
    startPoint = 0;
  }
  
  //Starting at increment so sequential segments don't intersect
  for(testSeg=INCREMENT; testSeg< 1.0; testSeg += INCREMENT)
  {
    float testSegX1, testSegY1, testSegX2, testSegY2;
    testBezierQuad(&testSegX1, &testSegY1, &testSegX2, &testSegY2, 
                   g_lines[testCurve][STR][X],
                   g_lines[testCurve][STR][Y],
                   g_lines[testCurve][MID][X],
                   g_lines[testCurve][MID][Y],
                   g_lines[testCurve][END][X],
                   g_lines[testCurve][END][Y], testSeg);
    
    for(i = startPoint; i != workingPoint; i = (i+1)%TARGET_LENGTH)
    {
      //go through each line segment being drawn in the testCurve,
      // compare it against all other segments that have been drawn
      // If they intersect, find the angle between the two lines
      //  fail if <80 or > 100
      

      for(targetSeg=0.0; targetSeg< 1.0; targetSeg += INCREMENT)
      {
        float X1, Y1, X2, Y2;
        testBezierQuad(&X1, &Y1, &X2, &Y2, 
                     g_lines[i][STR][X],
                     g_lines[i][STR][Y],
                     g_lines[i][MID][X],
                     g_lines[i][MID][Y],
                     g_lines[i][END][X],
                     g_lines[i][END][Y], targetSeg);
                     
        //Alright, so now we have 8 values, 4 points, 2 lines, which we compare for intersection and stuff.
        if(lineIntersect(testSegX1,testSegY1,testSegX2,testSegY2, X1,Y1,X2,Y2))
        {
          float segAngle = atan2(testSegY1 - testSegY2,testSegX1 - testSegX2)  * 180 / 3.14159;
          float angle = atan2(Y1-Y2,X1-X2)  *180 / 3.14159;
          
          if(segAngle - angle  < 80.0 ||  segAngle - angle > 100.0)
          {
            //sprintf(g_buff, "BAD: %f", segAngle - angle);
            return false;
          }
          else
          {
            sprintf(g_buff, "last good angle: %f", segAngle - angle);
          }
        }
      }
      //So the bezier curve algo I'm using runs this shenanigians again with targetSeg == exactly to 1.0 to make sure we draw a line to the endpoint. Ugh... repeating code like this make my blood boil
      targetSeg=1.0;
      
      float X1, Y1, X2, Y2;
      testBezierQuad(&X1, &Y1, &X2, &Y2, 
                   g_lines[i][STR][X],
                   g_lines[i][STR][Y],
                   g_lines[i][MID][X],
                   g_lines[i][MID][Y],
                   g_lines[i][END][X],
                   g_lines[i][END][Y], targetSeg);
                   
      //Alright, so now we have 8 values, 4 points, 2 lines, which we compare for intersection and stuff.
      if(lineIntersect(testSegX1,testSegY1,testSegX2,testSegY2, X1,Y1,X2,Y2))
      {
        float segAngle = atan2(testSegY1 - testSegY2,testSegX1 - testSegX2)  * 180 / 3.14159;
        float angle = atan2(Y1-Y2,X1-X2)  *180 / 3.14159;
        
        if(segAngle - angle  < 80.0 ||  segAngle - angle > 100.0)
        {
          //sprintf(g_buff, "BAD: %f", segAngle - angle);
          return false;
        }
        else
        {
          sprintf(g_buff, "last good angle: %f", segAngle - angle);
        }
      }
      //End stupid repeat code. 
    }
  }
  return true;
}

int pickPoint(int* targetX, int* targetY, int prevEndX, int prevEndY,  int midX, int midY)
{
  
  int tryLeftX, tryLeftY, tryRightX, tryRightY;
   
  //Find the point to curve right or left 
  tryLeftX = midX + (midY - prevEndY)  +rand()%21-10;
  tryLeftY = midY - (midX - prevEndX)  +rand()%21-10;
  
  tryRightX = midX - (midY - prevEndY) +rand()%21-10;
  tryRightY = midY + (midX - prevEndX) +rand()%21-10;
  
  //If one point is invalid, pick the other, if both invalid, FAILURE
  if( (tryLeftX < 0 || tryLeftX > SCREEN_WIDTH || tryLeftY < 0 || tryLeftY > SCREEN_WIDTH) &&
      (tryRightX < 0 || tryRightX > SCREEN_HEIGHT || tryRightY < 0 || tryRightY > SCREEN_HEIGHT))
  {
    return 0;
  }
  if(tryLeftX < 0 || tryLeftX > SCREEN_WIDTH || tryLeftY < 0 || tryLeftY > SCREEN_WIDTH)
  {
    *targetX = tryRightX;
    *targetY = tryRightY;
    return 1;
  }
  if(tryRightX < 0 || tryRightX > SCREEN_HEIGHT || tryRightY < 0 || tryRightY > SCREEN_HEIGHT)
  {
    *targetX = tryLeftX;
    *targetY = tryLeftY;
    return 1;
  }

  if(rand()%100/50)
  {
    *targetX = tryRightX;
    *targetY = tryRightY;
  }
  else
  {
    *targetX = tryLeftX;
    *targetY = tryLeftY;
  }
  
  return 1;
}



int displayInit()
{
  g_screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
  if( g_screen == NULL )
  {
    return false;
  }
  //Initialize SDL_ttf
  if( TTF_Init() == -1 )
  {
    return false;
  }
  g_font = TTF_OpenFont( "agencyfb-bold.ttf", 24);
  if( g_font == NULL )
  {
    return false;
  }
  
  return true;
}

int init()
{
  if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
  {
    return false;
  }
  
  
  if( displayInit() == false)
  {
    return false;
  }
  
  SDL_WM_SetCaption( "Doodle", NULL );
  
  g_seed = abs(time(NULL));
  srand(g_seed);
    
  //Initial start point
  g_lines[0][STR][X] = SCREEN_WIDTH/2;
  g_lines[0][STR][Y] = SCREEN_HEIGHT/2;
    
  g_lines[0][MID][X] = SCREEN_WIDTH/2 + 50 - rand()%100;
  g_lines[0][MID][Y] = SCREEN_HEIGHT/2 + 50 - rand()%100;
    
  g_lines[0][END][X] = SCREEN_WIDTH/2 + 50 - rand()%100;
  g_lines[0][END][Y] = SCREEN_HEIGHT/2 + 50 - rand()%100;
  
  //After the first point, we calculate it in the display loop (which is usually a HORRIBLE idea) because we want to see it process each step. Cause it's an introspective demo.
  
  
  
  g_startTime = SDL_GetTicks();
  
  return true;
}


const char help[]="\
[--help][-h] prints this and exits.\n";

void processArguments(int argc, char** argv)
{
	int c;	//Damn you, you terse C motherfuckers and your shitty terse examples

	opterr = 0;

	while ((c = getopt (argc, argv, "h")) != -1)
	{
		switch (c)
		{
		case 'h':	//help
			printf("%s",help);
			exit(0);
			break;
		    
		case '?':
			if (optopt == 'c')
			{
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			}
			/*else if (isprint (optopt))
			{
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			}*/
			else
			{
				fprintf (stderr,"Unknown option character `%x(%c)\n", optopt, optopt);
			}
			exit(1);
		default: 
			return;
		}
	}
}


void cleanUp()
{

  //Free the surfaces
  SDL_FreeSurface( g_screen );
  
  //Close the font that was used
  TTF_CloseFont( g_font );

  //Quit SDL_ttf
  TTF_Quit();
  
  SDL_Quit();
  exit(0);
}





void drawText(int x, int y, SDL_Color color, TTF_Font* font, const char* text, ...)
{
	va_list args;
  char buff[100];

	va_start (args, text);
	vsprintf (buff, text, args); //Yep, well this was specifically written for wrappers.
	va_end (args);
  
  g_message = TTF_RenderText_Solid( font, buff, color );
  
  SDL_Rect offset;
  SDL_Rect* clip = NULL;
  offset.x = x;
  offset.y = y;
  SDL_BlitSurface( g_message, clip, g_screen, &offset );
  
  SDL_FreeSurface(g_message);
}


void drawPixel(SDL_Surface* surface, Uint32 x, Uint32 y, Uint32 color)
{
  Uint32 bpp, offset;

  if(y >= SCREEN_HEIGHT) { return; }  //hmmmm, sorta ties it to g_screen... but meh
  if(x >= SCREEN_WIDTH) { return; }

  bpp = surface->format->BytesPerPixel;
  offset = surface->pitch*y + x*bpp;

  SDL_LockSurface(surface);
  memcpy(surface->pixels + offset, &color, bpp);
  SDL_UnlockSurface(surface);
  
}

void drawDot(SDL_Surface* surface, Uint32 x, Uint32 y, Uint32 color)
{
  drawPixel(surface,x,y, color);
  
  drawPixel(surface,x+1,y, color);
  drawPixel(surface,x,  y+1, color);
  drawPixel(surface,x-1,y, color);
  drawPixel(surface,x,  y-1, color);
  
  drawPixel(surface,x+2,y, color);
  drawPixel(surface,x,  y+2, color);
  drawPixel(surface,x-2,y, color);
  drawPixel(surface,x,  y-2, color);
  drawPixel(surface,x+1,y+1, color);
  drawPixel(surface,x+1,y-1, color);
  drawPixel(surface,x-1,y+1, color);
  drawPixel(surface,x-1,y-1, color);
}

void drawLine(SDL_Surface* surface, int x1,int y1,int x2,int y2, Uint32 color)
{
  int dx, dy, i, e;
  int incx, incy, inc1, inc2;
  int x,y;

  dx = x2 - x1;
  dy = y2 - y1;

  if(dx < 0) { dx = -dx; }
  if(dy < 0) { dy = -dy; }
  incx = 1;
  if(x2 < x1) { incx = -1; }
  incy = 1;
  if(y2 < y1) { incy = -1; }
  x=x1;
  y=y1;

  if(dx > dy)
  {
    drawPixel(surface,x,y, color);
    e = 2*dy - dx;
    inc1 = 2*( dy -dx);
    inc2 = 2*dy;
    for(i = 0; i < dx; i++)
    {
      if(e >= 0)
      {
        y += incy;
        e += inc1;
      }
      else 
      {
        e += inc2; 
      }
      x += incx;
      drawPixel(surface,x,y, color);
    }
  }
  else
  {
    drawPixel(surface,x,y, color);
    e = 2*dx - dy;
    inc1 = 2*( dx - dy);
    inc2 = 2*dx;
    for(i = 0; i < dy; i++)
    {
      if(e >= 0)
      {
        x += incx;
        e += inc1;
      }
      else 
      { 
        e += inc2; 
      }
      y += incy;
      drawPixel(surface,x,y, color);
    }
  }
}

void drawBox(SDL_Surface* surface, int x1,int y1,int x2,int y2, Uint32 color)
{
  drawLine(g_screen, x1, y1, x1, y2, color);
  drawLine(g_screen, x1, y2, x2, y2, color);
  drawLine(g_screen, x2, y1, x2, y2, color);
  drawLine(g_screen, x2, y1, x1, y1, color);
}


void pointOnLinef(float *targetX, float *targetY, float x1, float y1, float x2, float y2, float percent)
{
  *targetX = ((x2-x1) * percent) + x1;
  *targetY = ((y2-y1) * percent) + y1;
}

void pointOnLine(int *targetX, int *targetY, int x1, int y1, int x2, int y2, float percent)
{
  *targetX = ((x2-x1) * percent) + x1;
  *targetY = ((y2-y1) * percent) + y1;
}

int pointInLine(int *targetX, int *targetY, int x1, int y1, int x2, int y2, float percent)
{
  int x = 0;
  int y = 0;
  
  x = ((x2-x1) * percent) + x1;  
  *targetX = x;
  y = ((y2-y1) * percent) + y1;  
  *targetY = y;
  
  if(x <= 0 || x > SCREEN_WIDTH || y <= 0 || y > SCREEN_WIDTH)
    return 0;
  return 1;
  
}

//hmmmm, need to redo this as a variadic, so higher levels are just THERE
void drawBezierQuad(SDL_Surface* surface, Uint32 color, int startX,int startY,int midX,int midY, int endX, int endY)
{
  int line1x, line1y, line2x, line2y,  line3x, line3y;
  int oldx, oldy;
  float i;
  
  oldx = -1;
  oldy = -1;
  
  pointOnLine(&line1x, &line1y, startX, startY, midX, midY, 0);
  pointOnLine(&line2x, &line2y, midX, midY, endX, endY, 0);
  pointOnLine(&oldx, &oldy, line1x, line1y, line2x, line2y, 0);
  
  for(i=0; i <= 1.00; i = i + INCREMENT)
  {
    pointOnLine(&line1x, &line1y, startX, startY, midX, midY, i);
    pointOnLine(&line2x, &line2y, midX, midY, endX, endY, i);
    pointOnLine(&line3x, &line3y, line1x, line1y, line2x, line2y, i);
    
    //drawPixel(surface,line3x,line3y, color);
    drawLine(surface, oldx, oldy, line3x, line3y, color);
    
    oldx = line3x;
    oldy = line3y;
  }  

  //Ugh
  pointOnLine(&line1x, &line1y, startX, startY, midX, midY, 1.0);
  pointOnLine(&line2x, &line2y, midX, midY, endX, endY, 1.0);
  pointOnLine(&line3x, &line3y, line1x, line1y, line2x, line2y, 1.0);
  
  //drawPixel(surface,line3x,line3y, color);
  drawLine(surface, oldx, oldy, line3x, line3y, color);

}



Uint32 animation( )
{
  static int oldTime = 0;
  int i;


  if( SDL_GetTicks() - oldTime < 1000/ FRAMES_PER_SECOND)
  { return 0; }
  oldTime = SDL_GetTicks();

  //Fill the screen with white / erase the last frame
  SDL_FillRect(g_screen,NULL, 0x000000);

  drawText(12,12,textColor, g_font, g_buff);
  
  int startPoint = 0;
  if( infinateLoop) 
  {
    if(workingPoint == 98)      startPoint = 1;
    else if(workingPoint == 98) startPoint = 0;
    else                        startPoint = workingPoint + 2;
  }
  //TODO  still a display bug with backing up TWICE and displaying old data. Meh
  //drawLine(g_screen, g_lines[i][MID][X], g_lines[i][MID][Y], g_lines[i+1][MID][X], g_lines[i+1][MID][Y], 0x000000f0);
  //drawDot(g_screen, g_lines[i][END][X], g_lines[i][END][Y], 0x00FF00FF);
  //drawDot(g_screen, g_lines[i][MID][X], g_lines[i][MID][Y], 0x0000FF00);
  for( i = startPoint; i != workingPoint; i = (i+1)%TARGET_LENGTH )
  {
    drawBezierQuad( g_screen, 0xffffffff, g_lines[i][STR][X],g_lines[i][STR][Y],  g_lines[i][MID][X],g_lines[i][MID][Y],  g_lines[i][END][X],g_lines[i][END][Y]);
  }
  
  if( SDL_Flip( g_screen ) == -1 )
  {
    printf("UpdateFAIL\n");
  }
  return 1;
}



void control()
{
 
  static int oldTime = 0;
  //int speed;
  

  //Oh god, the logic of how the line is drawn is somehow tied to how fast this triggers..... wtf?
  if( SDL_GetTicks() - oldTime < 1000/ FRAMES_PER_SECOND)
  { return ; }
  oldTime = SDL_GetTicks();

  //After the first point, the mid-point for the curves must be on the same line to be smooth
  int prevPoint;
  if(workingPoint == 0) prevPoint = 99;
  else                  prevPoint = workingPoint-1;

  //Start point is the same as the previous end pint
  g_lines[workingPoint][STR][X] = g_lines[prevPoint][END][X];
  g_lines[workingPoint][STR][Y] = g_lines[prevPoint][END][Y];

  //Mid point must be on same line as prev mid and it's endpoint
  int j,k;
  int valid = 1;
  
  float randy = 1.5 + (float)(rand()%130)/100.0;    //Since this is finding the Midpoint distance from the previous end point to mid point, the distance has to be about double
  //huh, these numbers are fun to play with.
  
  valid = valid && pointInLine(&j, &k, g_lines[prevPoint][MID][X], 
                                        g_lines[prevPoint][MID][Y], 
                                        g_lines[prevPoint][END][X],
                                        g_lines[prevPoint][END][Y], randy);
                                        
  //Make sure it doesn't shrink away to nothing.
  float length = sqrt(abs(g_lines[prevPoint][MID][X] - g_lines[prevPoint][END][X]) *
                      abs(g_lines[prevPoint][MID][X] - g_lines[prevPoint][END][X]) +
                      abs(g_lines[prevPoint][MID][Y] - g_lines[prevPoint][END][Y]) *    
                      abs(g_lines[prevPoint][MID][Y] - g_lines[prevPoint][END][Y]));
  //drawText(10,10, textColor, g_font, "Length: %3.1f \t %d %d ", length, prevPoint, workingPoint);
  if(length < 10)
  {
    valid = 0;
  }
                                        
  if(valid)
  {
    g_lines[workingPoint][MID][X] = j;
    g_lines[workingPoint][MID][Y] = k;
    //End point is placed so that we avoid cluttered messes.
    if(pickPoint(&g_lines[workingPoint][END][X],
                 &g_lines[workingPoint][END][Y],  
                 g_lines[workingPoint][STR][X],
                 g_lines[workingPoint][STR][Y],  
                 g_lines[workingPoint][MID][X],
                 g_lines[workingPoint][MID][Y]) == 0)
    {
      valid = 0;
    }    
  }

  if(valid)
  {
    if( validateCurve(workingPoint) == 0)
    {
      valid = 0;
    }
  }
  
  
  if(valid)
  {
    workingPoint++;
    if(workingPoint == TARGET_LENGTH)
    {
      infinateLoop = 1;
      workingPoint = 0;
    }
  }
  else  
  {
    if(workingPoint == 0) 
    {
      workingPoint = 99; //TODO, Crashes when it hits an invalid point during this crossover
    }
    else
    {
      workingPoint--;
      if(workingPoint == -1)
        workingPoint = 99;
      valid = 1;  //RESET, try again next time
    }
  }

}




//Called in Main
int handleKeyStrokeDown()
{
  //char c;
  int sym = g_event.key.keysym.sym;
  //int mod = g_event.key.keysym.mod;

  switch(sym)
  {
  case SDLK_ESCAPE:
    cleanUp();
    exit(0);
  
  default:
    ;
  }
  return 0;
}

int handleEvent(int eventType)
{
  switch(eventType)
  {
  case SDL_QUIT:
    cleanUp();
    exit(0);
    return true;
    break;

  case SDL_KEYDOWN:
    handleKeyStrokeDown();
    break;
    
  case SDL_KEYUP:
    //handleKeyStrokeUp();
    break;

  case SDL_MOUSEBUTTONDOWN:
    if( g_event.button.button == 4) //mousewheel down
    {
      g_wheel--;
    }
    if( g_event.button.button == 5) //mousewheel up
    {
      g_wheel++;
    }

  default:
    ;
  }
  return false;
}


// MAIN!
int main( int argc, char* args[] )
{
  int quit = false;
  
  processArguments(argc, args);
  
  if( init() == false )
  {
    return 1;
  }

  while( quit == false ) //Main loop
  {  
    while( SDL_PollEvent( &g_event ) )
    {
      quit = handleEvent(g_event.type);
    }
    animation();
    control();
  }
  cleanUp();

  return 0;
}
