all: main.c 
	gcc -o doodle.exe main.c -lmingw32 -lSDLmain -lSDL -lSDL_ttf -g -Wall

linux: main.c 
	gcc -o doodle main.c -lm -lSDLmain -lSDL -lSDL_ttf -g -Wall

	
clean : 
	rm *.o
	rm doodle.exe
	

  
